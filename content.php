<?php
// Pagina de categorias. Looping.

get_header(); 

$random = rand (0,9);
?>
<div class="banners-pages" style="background:url(<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/head-<?php echo $random; ?>.jpg);">
</div>
	<!-- blog-page -->
	<div class="blog">
		<div class="container">
			<div class="blog-head">
				<h2><?php the_archive_title(); ?></h2>
			</div>
			<div class="col-md-8 blog-left" >
				<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php 
							$format = get_post_format();
							
							if ( false === $format ){
								$format = 'standard';
							}
						?>
						<?php get_template_part( 'content', $format ); ?>
					<?php endwhile; ?>
					<?php 
						the_posts_pagination( array(
							'prev_text'          => __( '«', 'foroliberal' ),
							'next_text'          => __( '»', 'foroliberal' ),
						) );
					?>
				<?php else : ?>
					<?php get_template_part( 'content', 'none' ); ?>
				<?php endif; ?>
			</div>	
			<div class="col-md-4 main-sidebar">
				<?php
					if ( is_active_sidebar( 'main-post-sidebar' ) ) :
							dynamic_sidebar( 'main-post-sidebar' );
					endif;
				?>
			</div>
			<div class="clearfix"></div>
		</div>	
	</div>	
	<!--//blog-->
<?php get_footer(); ?>