<!---->
<div class="footer">
	 <div class="container">
		 <div class="ftr-sec">
			 <div class="col-md-4 ftr-grid">
			 	<?php if ( is_active_sidebar( 'foot-left-sidebar' ) ) : ?>
						<?php dynamic_sidebar( 'foot-left-sidebar' ); ?>
				<?php endif; ?>
			 </div>
			 <div class="col-md-4 ftr-grid">
			 	<?php if ( is_active_sidebar( 'foot-center-sidebar' ) ) : ?>
						<?php dynamic_sidebar( 'foot-center-sidebar' ); ?>
				<?php endif; ?>
			 </div>
			 <div class="col-md-4 ftr-grid">
			 	<?php if ( is_active_sidebar( 'foot-right-sidebar' ) ) : ?>
						<?php dynamic_sidebar( 'foot-right-sidebar' ); ?>
				<?php endif; ?>
			 </div>
			 <div class="clearfix"></div>
		 </div>
	 </div>
</div>
<!---->
<div class="copywrite"> 
	 <div class="container">
		 <p>Copyright © 2015 Foro Liberal de America Latina. <br />Todos Los Derechos Reservados <br />Mantenido por <a href="/quienes-somos">un equipo de liberales.</a></p>
	 </div>
</div>
<!---->
</body>
</html>