<?php


/**
 * Init del tema.
 */
function liberty_init() {

	//main Sidebar
	register_sidebar( array(
		'name'          => __( 'Main Post Sidebar Area', 'foro-liberal' ),
		'id'            => 'main-post-sidebar',
		'description'   => __( 'Estes el panel lateral que aparece en los posts.', 'foro-liberal' ),
		'before_widget' => '<div id="%1$s" class="main-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );

	//Post Sidebar
	register_sidebar( array(
		'name'          => __( 'Post Sidebar Area', 'foro-liberal' ),
		'id'            => 'post-sidebar',
		'description'   => __( 'Estes el panel lateral que aparece en los posts.', 'foro-liberal' ),
		'before_widget' => '<div id="%1$s" class="main-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'          => __( 'CLC Post Sidebar Area', 'foro-liberal' ),
		'id'            => 'clc-post-sidebar',
		'description'   => __( 'Estes el panel lateral que aparece en los posts del Centro de Liberalismo Clasico.', 'foro-liberal' ),
		'before_widget' => '<div id="%1$s" class="main-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'          => __( 'Instituto Crisologo Barron Post Sidebar Area', 'foro-liberal' ),
		'id'            => 'instituto-post-sidebar',
		'description'   => __( 'Estes el panel lateral que aparece en los posts del Instituto Crisologo Barron.', 'foro-liberal' ),
		'before_widget' => '<div id="%1$s" class="main-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );

	//Categories Side Bar

	//Footer Bars
	register_sidebar( array(
		'name'          => __( 'Footer Izquierda', 'foro-liberal' ),
		'id'            => 'foot-left-sidebar',
		'description'   => __( 'Pie a la izquierda.', 'foro-liberal' ),
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer Centro', 'foro-liberal' ),
		'id'            => 'foot-center-sidebar',
		'description'   => __( 'Pie al centro.', 'foro-liberal' ),
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer Derecha', 'foro-liberal' ),
		'id'            => 'foot-right-sidebar',
		'description'   => __( 'Pie a la derecha.', 'foro-liberal' ),
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

	//Home Bars
	register_sidebar( array(
		'name'          => __( 'Instituto Crisologo Barron', 'foro-liberal' ),
		'id'            => 'instituto-sidebar',
		'description'   => __( 'Aca va la info a colocar en instituto crisologo barron.', 'foro-liberal' ),
		'before_widget' => '<div id="%1$s" class="main-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'          => __( 'Radio y Prensa', 'foro-liberal' ),
		'id'            => 'press-sidebar',
		'description'   => __( 'Aca va la info a colocar en radio y prensa.', 'foro-liberal' ),
		'before_widget' => '<div id="%1$s" class="main-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'          => __( 'CLC', 'foro-liberal' ),
		'id'            => 'clc-sidebar',
		'description'   => __( 'Aca va la info para el centro de liberalismo clasico.', 'foro-liberal' ),
		'before_widget' => '<div id="%1$s" class="main-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );

	register_nav_menus( array(
		'primary' => __( 'Menu Principal',      'twentyfifteen' ),
		'social'  => __( 'Redes Sociales', 'twentyfifteen' ),
	) );

	add_theme_support( 'post-formats', array(
		'image', 'video', 'quote', 'link', 'gallery', 'audio'
	) );

	add_theme_support( 'post-thumbnails' );


	// Creamos las categorias principales
	wp_insert_term(
		'CLC',
		'category',
		array(
		  'description'	=> 'Publicaciones relacionadas al Centro de Liberalismo Clasico.',
		  'slug' 		=> 'clc'
		)
	);

	wp_insert_term(
		'Instituto Crisólogo Barrón',
		'category',
		array(
		  'description'	=> 'Publicaciones relacionadas al Instituto Crisólogo Barrón',
		  'slug' 		=> 'instituto'
		)
	);

	wp_insert_term(
		'Radio y Prensa',
		'category',
		array(
		  'description'	=> 'Publicaciones relacionadas con la Radio y Prensa',
		  'slug' 		=> 'press'
		)
	);

	wp_insert_term(
		'Destacados',
		'category',
		array(
		  'description'	=> 'Publicaciones destacadas para mostrar en el slider principal.',
		  'slug' 		=> 'destacados'
		)
	);

	// Creamos la categoria de publicaciones
	wp_insert_term(
		'Publicaciones',
		'category',
		array(
		  'description'	=> 'Publicaciones en general del blog del foro liberal de america latina.',
		  'slug' 		=> 'publicaciones'
		)
	);
	
	$term = term_exists( 'publicaciones', 'category' );
	$cat_id = $term['term_id'];

	// Creamos las subcategorias de publicaciones.
	wp_insert_term(
		'Artículos',
		'category',
		array(
		  'description'	=> 'Artículos en general del blog del foro liberal de america latina.',
		  'slug' 		=> 'articulos',
		  'parent'		=> $cat_id
		)
	);

	wp_insert_term(
		'Ensayos',
		'category',
		array(
		  'description'	=> 'Ensayos en general del blog del foro liberal de america latina.',
		  'slug' 		=> 'ensayos',
		  'parent'		=> $cat_id
		)
	);
	
	wp_insert_term(
		'Libros',
		'category',
		array(
		  'description'	=> 'Libros en general del blog del foro liberal de america latina.',
		  'slug' 		=> 'libros',
		  'parent'		=> $cat_id
		)
	);

	wp_insert_term(
		'Audios',
		'category',
		array(
		  'description'	=> 'Audios en general del blog del foro liberal de america latina.',
		  'slug' 		=> 'audios',
		  'parent'		=> $cat_id
		)
	);

	wp_insert_term(
		'Videos',
		'category',
		array(
		  'description'	=> 'Videos en general del blog del foro liberal de america latina.',
		  'slug' 		=> 'videos',
		  'parent'		=> $cat_id
		)
	);

	wp_insert_term(
		'Eventos',
		'category',
		array(
		  'description'	=> 'Eventos en general del blog del foro liberal de america latina.',
		  'slug' 		=> 'eventos',
		  'parent'		=> $cat_id
		)
	);

	wp_insert_term(
		'Miscelaneas',
		'category',
		array(
		  'description'	=> 'Publicaciones miscelaneas del blog del foro liberal de america latina.',
		  'slug' 		=> 'miscelaneas',
		  'parent'		=> $cat_id
		)
	);

}
add_action( 'after_setup_theme', 'liberty_init' );


// shortcodes
function shortcode_radio_liberal() {

?>
	<!--Codigo V1 www.desafiohosting.com v1.0 -->
	<script src="http://tienda.desafiohosting.com/downloads/generador-reproductores/reproductores/movil/mediaelement-and-player.min.js"></script>
	<link rel="stylesheet" href="http://tienda.desafiohosting.com/downloads/generador-reproductores/reproductores/movil/mediaelementplayer.min.css">
	<audio id="radio-player" src="http://server2.crearradio.com:8300/;stream" type="audio/mp3"></audio>	
	<div id="radio-controller-box">
		<div id="radio-controller-wrap">
			<div id="radio-controller-title">radio 5r</div>
		</div>
		<div id="radio-controller-wrap">
			<div id="radio-controller" class="radio-start">(( escuchar ))</div>
		</div>
	</div>
	<script>
		// JavaScript object for later use
		var player = new MediaElementPlayer('#radio-player',{
		    // width of audio player
		    audioWidth: 320,
		    // height of audio player
		    audioHeight: 30,
		    // initial volume when the player starts
		    startVolume: 0.8,
		    // enables Flash and Silverlight to resize to content size
		    enableAutosize: true,
		    // the order of controls you want on the control bar (and other plugins below)
		    features: ['playpause','current','volume'],
		    // force iPad's native controls
		    iPadUseNativeControls: false,
		    // force iPhone's native controls
		    iPhoneUseNativeControls: false, 
		    // force Android's native controls
		    AndroidUseNativeControls: false,
		    // turns keyboard support on and off for this instance
		    enableKeyboard: true,
		    // when this player starts, it will pause other players
		    pauseOtherPlayers: true,
		    // array of keyboard commands
		    keyActions: []
		});

		$(document).on('click','#radio-controller', function(){
			var stopImg = '- detener -';
			var startImg = '(( escuchar ))';

			if($(this).hasClass('radio-start')){
				$(this).text(stopImg);
				$(this).removeClass('radio-start');
				$(this).addClass('radio-stop');
				player.play();
			}else{
				$(this).text(startImg);
				$(this).removeClass('radio-stop');
				$(this).addClass('radio-start');
				player.pause();
			}	
		});
	</script>
	<!-- fin de Codigo V1 www.desafiohosting.com v1.0  -->
<?php

}
add_shortcode( 'radio', 'shortcode_radio_liberal' );



function shortcode_podcast_ivoox( $atts ) {
	// Attributes
	extract($atts);
?>
	<iframe 
		id='<?php echo $audioid; ?>' 
		frameborder='0' 
		allowfullscreen='' 
		scrolling='no' 
		height='200' 
		class="podcast-xlarge"
		src="https://www.ivoox.com/<?php echo $audiopage; ?>?c1=5E91F7">
	</iframe>
<?php
}
add_shortcode( 'podcast-ivoox', 'shortcode_podcast_ivoox' );

function frontPageWhereClausule(){
	$obj = get_category_by_slug('articulos');
if($obj!=null){
	$catArticlesId = $obj->term_id;	
}

	$obj = get_category_by_slug('ensayos'); 
	if($obj!=null){
		$catEnsayId = $obj->term_id;
	}

	$obj = get_category_by_slug('audios'); 
	if($obj!=null){
		$catAudioId = $obj->term_id;
	}

	$obj = get_category_by_slug('videos'); 
	if($obj!=null){
		$catVideoId = $obj->term_id;
	}

	$obj = get_category_by_slug('citas'); 
	if($obj!=null){
		$catCiteId = $obj->term_id;
	}

	$where = 'cat=';

	if(isset($catArticlesId)){
		$where .= $catArticlesId;
		$whereIsSet = true;
	}

	if(isset($catEnsayId) && $whereIsSet){
		$where .= ','.$catEnsayId;
	}else{
		if(isset($catEnsayId)){
			$where .= $catEnsayId;
			$whereIsSet = true;
		}
	}

	if(isset($catAudioId) && $whereIsSet){
		$where .= ','.$catAudioId;
	}else{
		if(isset($catAudioId)){
			$where .= $catAudioId;
			$whereIsSet = true;
		}
	}

	if(isset($catVideoId) && $whereIsSet){
		$where .= ','.$catVideoId;
	}else{
		if(isset($catVideoId)){
			$where .= $catVideoId;
			$whereIsSet = true;
		}
	}

	if(isset($catCiteId) && $whereIsSet){
		$where .= ','.$catCiteId;
	}else{
		if(isset($catCiteId)){
			$where .= $catCiteId;
			$whereIsSet = true;
		}
	}

	return $where;
}

// Add Shortcode
function shortcode_followme( $atts , $content = null ) {

	// Attributes
	//extract($atts);
?>
	<ul class="icons-large">
		<?php do_shortcode($content); ?>
	</ul>
<?php	
}
add_shortcode( 'followme', 'shortcode_followme' );

// Add Shortcode
function shortcode_followme_item( $atts ) {

	// Attributes
	extract($atts);
?>
	<li class="<?php echo $type; ?>">
		<a href="<?php echo $link; ?>" target="_blank">
			<img class="site-icon" src="<?php echo $image; ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>" height="64" width="64" />
			<span style=" background: none; display: inline; padding-left: 1em;"><?php echo $title; ?></span>
		</a>
	</li>
<?php
}
add_shortcode( 'followme_item', 'shortcode_followme_item' );
add_filter('widget_text', 'do_shortcode');

function sv_remove_jp_sharing() {
    if ( is_singular( 'post' ) && function_exists( 'sharing_display' ) ) {
        remove_filter( 'the_content', 'sharing_display', 19 );
        remove_filter( 'the_excerpt', 'sharing_display', 19 );
    }
}
add_action( 'loop_start', 'sv_remove_jp_sharing' );
?>