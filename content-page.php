<div class="blog-info">
	<div class="blog-head">
		<h2><?php the_title(); ?></h2>
	</div>
	<div class="post">
		<?php the_content(); ?>
	</div>
</div>
