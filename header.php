<?php
// Este archivo contiene toda la cabecera del tema para el foro liberal
// la carga de los scripts, jquery y estilos se llaman desde aca, esta es
// la cabecera unica para todas las paginas del sitio web.
?>
<!--
	Inicio del header
-->
<!DOCTYPE HTML>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<title><?php bloginfo( 'title' ); ?></title>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<!-- 
			jQuery Framework
		-->
		<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/js/jquery.min.js"></script>
		<!-- end -->
		<!-- 
			Bootstrap Framework
		 -->
			<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
			<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/js/bootstrap.js"></script>
		<!-- end -->
		<!-- <script type="application/x-javascript"> 
			addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); 
			function hideURLbar(){ window.scrollTo(0,1); } 
		</script> -->
		<!-- 
			Dynamic Menu
		 -->
		<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/js/dynamicMenu.js"></script>
		<!-- end -->
		<!-- 
			Theme Styles
		 -->
			<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
		<!-- end -->
		<?php wp_head(); ?>
	</head>

	<body>

	<div class="header">
		<div class="logo">
			<a href="/"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/logo.png" alt=""/></a>
		</div>
		<div class="top-menu">
			<span class="menu"></span>
			<?php if ( has_nav_menu( 'primary' ) ) : ?>
				<?php
				// Menu primario de navegacion
				wp_nav_menu( array(
					'menu_class'     => 'navig dropdown',
					'theme_location' => 'primary',
				) );
			?>
			<!-- fin-menu -->
			<?php endif; ?>
		</div>
		
		<!-- script-for-menu -->
		<script>
			$("span.menu").click(function(){
				$("ul.navig").slideToggle("slow" , function(){
				});
			});
		</script>
		<!-- script-for-menu -->

		<div class="clearfix"></div>
	</div>

