<?php
// Archivo principal del tema
	get_header();
?>

<!-- sliders -->
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/js/responsiveslides.min.js"></script>
	<script>  
		$(function () {
			$("#slider").responsiveSlides({
				auto: true,
				nav: true,
				speed: 700,
				namespace: "callbacks",
				pager: true,
				pauseControls: true
			});
			$(".slider-inner").width(window.width);
		});
	</script>  
	<?php
		$random = rand (0,9);
		$obj = get_category_by_slug('destacados'); 
  		$catFeaturedId = $obj->term_id;

  		$query = new WP_Query( 'cat='.$catFeaturedId );
	?>
	<div class="slider">
		<div class="slider-inner">
			<div class="caption ">
				<div class="callbacks_container">
					<ul class="rslides" id="slider">
						<?php if ( $query->have_posts() ) : ?>
							<?php while ( $query->have_posts() ) : $query->the_post(); ?>
								<?php if ( has_post_thumbnail() ) : ?>
									<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
									<li style="background-image: url(<?php echo $feat_image; ?>)">
										<h3 class="main-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
									</li>
								<?php else : ?>
									<li style="background-image: url(<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/<?php echo $random; ?>.jpg)">
												<h3 class="main-title"><a href="/quienes-somos">Foro Liberal de America Latina</a></h3>
									</li>
								<?php endif; ?>
							<?php endwhile; ?>
						<?php else : ?>
							<li style="background-image: url(<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/<?php echo $random; ?>.jpg)">
								<h3 class="main-title"><a href="/quienes-somos">Foro Liberal de America Latina</a></h3>
							</li>
						<?php endif; ?>
					</ul>	
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	  
	<div class="banner-grids">
		<div class="container">
			<div class="col-md-4 banner-grid">
				<h3><a href="category/press/">Radio y Prensa</a></h3>
				<div class="banner-grid-sec">
					<?php if ( is_active_sidebar( 'press-sidebar' ) ) : ?>
							<?php dynamic_sidebar( 'press-sidebar' ); ?>
					<?php endif; ?>
				</div>
			</div>

			<div class="col-md-4 banner-grid">
				<h3><a href="category/clc/">Centro de Liberalismo Clásico</a></h3>
				<div class="banner-grid-sec">
					<?php if ( is_active_sidebar( 'clc-sidebar' ) ) : ?>
							<?php dynamic_sidebar( 'clc-sidebar' ); ?>
					<?php endif; ?>
				</div>
			</div>

			<div class="col-md-4 banner-grid">
				<h3><a href="category/instituto/">Instituto Crisólogo Barrón</a></h3>
				<div class="banner-grid-sec">
					<?php if ( is_active_sidebar( 'instituto-sidebar' ) ) : ?>
							<?php dynamic_sidebar( 'instituto-sidebar' ); ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	
	<!-- end for the fold grids-->
	<?php
		
		$where = frontPageWhereClausule();

  		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

  		$query = new WP_Query( $where . '&posts_per_page=6&paged=' . $paged);
	?>
	<div class="row">
		<div class="container">
			<div class="col-md-12 blue-head center">
				<h2>Aprenda más sobre el Liberalismo Clásico</h2>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="container">
			<div class="col-md-8">
				<?php if ( $query->have_posts() ) : ?>
					<?php while ( $query->have_posts() ) : $query->the_post(); ?>
						<?php 
							$format = get_post_format();
							
							if ( false === $format ){
								$format = 'standard';
							}
						?>
						<?php get_template_part( 'content', $format ); ?>
					<?php endwhile; ?>
					<?php 
						the_posts_pagination( array(
							'prev_text'          => __( '«', 'foroliberal' ),
							'next_text'          => __( '»', 'foroliberal' ),
						) );
					?>
				<?php else : ?>
					<?php get_template_part( 'content', 'none' ); ?>
				<?php endif; ?>
			</div>
			<div class="col-md-4 main-sidebar">
				<?php
					if ( is_active_sidebar( 'main-post-sidebar' ) ) :
							dynamic_sidebar( 'main-post-sidebar' );
					endif;
				?>
			</div>
		</div>
	</div>

<?php 
	get_footer(); 
?>
