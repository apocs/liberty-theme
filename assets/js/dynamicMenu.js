// Aca se regenera el menu default de wordpress.


$(document).ready(function(){
	
	var mainMenu = $('.menu-main-menu-container');
	var menuList = mainMenu.find('ul#menu-main-menu');
	menuList.find('li').each(function(i,e){
		
		if( $(this).hasClass( 'menu-item-has-children' ) ){
			var btnGroup = $('<div />').addClass('btn-group')
			var oldLink = $(this).children('a');
			var dropDownLink = $('<a />').prop({href:oldLink.prop('href')});
			dropDownLink.text( oldLink.text() );
			dropDownLink.attr('data-toggle','dropdown');
			dropDownLink.attr('data-hover','dropdown');
			
			var subList = $(this).children('ul');
			var caret = $('<b />').addClass('caret');

			if( subList.hasClass( 'sub-menu' ) ){
				subList.removeClass( 'sub-menu' );
				subList.addClass( 'dropdown-menu' );
			}

			dropDownLink.append(caret);

			btnGroup.append(
				dropDownLink,
				subList
			);

			$(this).html(btnGroup);
		}
	});

	$('.top-menu').slideDown('slow');
});