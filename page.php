<?php

get_header(); 
$random = rand (0,9);

?>
<div class="banners-pages" style="background:url(<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/head-<?php echo $random; ?>.jpg);">
</div>
<div class="blog">
	<div class="container">
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		// End the loop.
		endwhile;
		?>
	</div>	
</div>
<?php get_footer(); ?>