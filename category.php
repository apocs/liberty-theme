<?php
// Pagina de categorias. Looping.

get_header(); 

$random = rand (0,9);
?>
<div class="banners-pages" style="background:url(<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/head-<?php echo $random; ?>.jpg);">
</div>
	<!-- blog-page -->
	<div class="blog">
		<div class="container">
			<div class="blog-head">
				<h2><?php echo single_cat_title( '', false ); ?></h2>
			</div>
			<div class="col-md-8 blog-left" >
				<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php 
							$format = get_post_format();
							
							if ( false === $format ){
								$format = 'standard';
							}
						?>
						<?php get_template_part( 'content', $format ); ?>
					<?php endwhile; ?>
					<?php 
						the_posts_pagination( array(
							'prev_text'          => __( '«', 'foroliberal' ),
							'next_text'          => __( '»', 'foroliberal' ),
						) );
					?>
				<?php else : ?>
					<?php get_template_part( 'content', 'none' ); ?>
				<?php endif; ?>
			</div>	
			<div class="col-md-4 main-sidebar">
				<?php 
					$cat = get_query_var('cat');

  					$category = get_category ($cat);
  					
  					if ( is_active_sidebar( 'post-sidebar' ) ) :
						dynamic_sidebar( 'post-sidebar' );
					endif;

  					if( $category->slug == 'instituto' ) :
  						if ( is_active_sidebar( 'instituto-post-sidebar' ) ) :
  							dynamic_sidebar( 'instituto-post-sidebar' );
  						endif;
  					elseif( $category->slug == 'clc' ) :
  						if ( is_active_sidebar( 'clc-post-sidebar' ) ) :
  							dynamic_sidebar( 'clc-post-sidebar' );
  						endif;
  					endif;
				?>
			</div>
			<div class="clearfix"></div>
		</div>	
	</div>	
	<!--//blog-->
<?php get_footer(); ?>