<?php 
	$content = explode("|", get_the_content());
?>
<div class="blog-info">
	<h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
	<div class="blog-info-text">
		<?php echo $content[2]; ?>
	</div>
	<a href="<?php echo $content[0]; ?>" target="_blank" class="btn btn-primary hvr-rectangle-in"><?php echo $content[1]; ?></a>	
</div>