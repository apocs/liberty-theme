<div class="blog-info">
	<h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
	<?php $author = get_the_author(); ?>
	<p>Por: <?php echo $author; ?> &nbsp;&nbsp; Publicado el: <?php the_time('m - j - Y'); ?></p>
	<div class="blog-info-text">
		<?php the_content(); ?>
	</div>	
</div>