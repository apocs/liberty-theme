<div class="blog-info">
	<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

		<p><?php printf( __( '¿Listo para hacer tu primera publicacion? <a href="%1$s">comienza aqui.</a>.', 'foro-liberal' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

	<?php elseif ( is_search() ) : ?>

		<p><?php _e( 'Lo siento, no hemos podido encontrar lo que estabas buscando, prueba con otros terminos de busqueda.', 'foro-liberal' ); ?></p>
		<?php get_search_form(); ?>

	<?php else : ?>

		<p><?php _e( 'No hemos publicado nada aun en esta seccion, ¡Lo sentimos!', 'foro-liberal' ); ?></p>

	<?php endif; ?>
</div>