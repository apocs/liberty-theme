<div class="blog-info">
	<blockquote class="quotation">
		<?php the_content(); ?>
		<cite>~ <?php the_title(); ?></cite>
	</blockquote>
</div>